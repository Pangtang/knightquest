from setuptools import setup, find_packages

setup(
    name='knightquest',
    version='1.0.0',
    packages=find_packages(),
    install_requires=['pygame', 'numpy'],
    author='anthony blanchflower',
    author_email='anthonyblanchflower@btinternet.com',
    url='https://github.com/anthonyblanchflower',
    description='Dungeon exploration game',
    long_description=open('README.md').read(),
    package_data={'knightquest/data': ['*'], 'knightquest/assets': ['*']},
    include_package_data=True,
    classifiers=[
        'Environment :: MacOS X',
        'Environment :: Win32 (MS Windows)',
        'Environment :: X11 Applications',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: MacOS :: MacOS X',
        'Operating System :: Microsoft :: Windows',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 2']
)
