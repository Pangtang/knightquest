# KnightQuest

A tiny dungeon exploration game

## Getting Started

### Requirements

#### Pre-requisites

python 2.7
pip

#### Installing pygame

```sudo apt-get install python-dev libsdl-image1.2-dev libsdl-mixer1.2-dev libsdl-ttf2.0-dev   libsdl1.2-dev```
 ```libsmpeg-dev python-numpy subversion libportmidi-dev ffmpeg libswscale-dev libavformat-dev libavcodec-dev```

### Installing knightquest

```pip install ./knightquest```

### Launching knightquest

```python knightquest/Knightquest.py```

## Playing The Game

Zod has mislaid the Ring Of Yendor (again). As his faithful servant you have been sent down to the catacombs
to find the accursed relic.

Your character has three attributes: Skill (fighting prowess), Stamina (health), Luck (chances to flee or fight better).

Monsters inhabit the catacombs and will block your progress. Fight them to move on and score points (or just flee).

Find and interact with the Ring Of Yendor to end the game (and return to Zod).

Interact with magic wells to gain strength (or suffer terrible sickness).

Interact with potions to gain luck.

Interact with herbs to gain stamina (ahem).

Encounters with monsters and items will award your character score points. As you accrue these points your
characters skill will improve.

Control your character with the arrow keys (to turn or move forward/ back). Press any other key to enter the game
menu. Move in a monster encounter to fight (unless fleeing) or choose Fight from the game menu.
